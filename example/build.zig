const std = @import("std");
const ld = @import("loop-dir");
const Flags = ld.cc.Flags;

const cflags: Flags = &.{ "-std=c2x", "-Wall", "-Werror" };
const cppflags: Flags = &.{ "-std=c++23", "-Wall", "-Werror", "-Weffc++" };

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    b.top_level_steps.clearRetainingCapacity();

    const example_tls = b.step("example", "Build example");
    b.default_step = example_tls;

    const config: ld.Config = .{
        .target = target,
        .optimize = optimize,
        .tls_depth = 4,
        .datalist = &.{
            .{ .c, @ptrCast(&cflags) },
            .{ .cc, @ptrCast(&cppflags) },
        },
    };

    // compile two projects in the 'projects' directory only
    const buildDirZigAndCargo = ld.mconcat(.{ ld.buildDirZig, ld.buildDirRust });
    const build_dir = ld.loop(buildDirZigAndCargo, b, "projects/", .{ .target = target, .optimize = optimize }).?;

    example_tls.dependOn(build_dir);

    // recursively compile all projects in the 'recursive' directory, including subdirectories
    const build_rec = ld.loop(ld.buildAll, b, "recursive/", config).?;
    example_tls.dependOn(build_rec);

    // custom build commands for specific file types
    const ghc = ld.curry(ld.command.file, ".hs: ghc $input -o $output -no-keep-hi-files -no-keep-o-files");
    const sort = ld.curry(ld.command.file, "!.txt: sort $input -o $output");
    const buildFileCustom = ld.mconcat(.{ ghc, sort });
    const build_custom = ld.loop(buildFileCustom, b, "custom/", .{}).?;

    example_tls.dependOn(build_custom);

    // custom install prefix to define the output directory for compiled binaries
    const install_c = ld.basic(ld.buildFileC, b, "recursive/1.c", "custom/1", config);
    const install_zig = ld.basic(ld.buildFileZig, b, "recursive/1/2/3.zig", "custom/3", config);
    const install_make = ld.basic(ld.buildDirMake, b, "recursive/1/2/3/4.make/", "make", .{});

    example_tls.dependOn(install_c);
    example_tls.dependOn(install_zig);
    example_tls.dependOn(install_make);
}
