const std = @import("std");
const fs = std.fs;
const mem = std.mem;
const Build = std.Build;
const Step = Build.Step;
const String = []const u8;
const Options = @import("../build.zig").Options;

/// file :: String -> BuildFn
/// Compiles a single source file as an application.
///
/// command syntax:
/// [!]<src ext>: <compiler> [arg]... <$input> <$output> [$cache_dir]
/// e.g.:
/// .hs: ghc $input -o $output -outputdir $cache_dir
/// !.txt: sort $input -o $output
///
/// On Windows, compiler commands automatically append '.exe' to output files.
/// The '!' prefix in the command indicates a non-compiler command, preventing
/// the automatic addition of the '.exe' extension.
pub fn file(cmd: String, owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .file) return null;
    const colon = mem.indexOfScalar(u8, cmd, ':').?;
    const src_ext, const exe: bool = if (cmd[0] != '!') .{ cmd[0..colon], true } else .{ cmd[1..colon], false };
    if (!mem.eql(u8, fs.path.extension(opt.name), src_ext)) return null;

    const target = opt.build.target orelse owner.graph.host;
    const exe_file_ext = if (exe) target.result.exeFileExt() else "";
    const bin_name = owner.fmt("{s}{s}", .{ opt.output_name, exe_file_ext });
    const file_path = owner.pathJoin(&.{ opt.sub_path, opt.name });

    return compile(cmd[colon + 1 ..], owner, opt.dst_path orelse opt.sub_path, .{
        .input = file_path,
        .output = bin_name,
        .cache_dir = owner.pathJoin(&.{ owner.cache_root.path.?, src_ext, opt.sub_path, opt.output_name }),
    });
}

/// Compiles a single source file as an application.
/// command syntax:
/// <compiler> [arg]... <$input> <$output>
pub fn compile(cmd: String, owner: *Build, dst_path: String, s: struct {
    input: String,
    output: String,
    cache_dir: String,
}) *Step {
    if (mem.indexOfAny(u8, cmd, "<>") != null)
        @panic("Redirection symbols '<' and '>' are not supported.");

    var it = mem.tokenizeAny(u8, cmd, " \r\n");
    const compiler_name = it.next().?;
    const compiler = owner.addSystemCommand(&.{compiler_name});
    var output: Build.LazyPath = undefined;

    while (it.next()) |token| {
        // normal arguments
        if (token[0] != '$') {
            compiler.addArg(token);
            continue;
        }

        // parse variables
        const variable = token[1..];
        if (mem.eql(u8, variable, "input")) {
            compiler.addFileArg(owner.path(s.input));
        } else if (mem.eql(u8, variable, "output")) {
            output = compiler.addOutputFileArg(s.output);
        } else if (mem.eql(u8, variable, "cache_dir")) {
            compiler.addArg(s.cache_dir);
        } else {
            std.log.err("No variable named '{s}' found.", .{token});
            @panic("invalid token");
        }
    }

    compiler.setName(owner.fmt("{s} {s}", .{ compiler_name, s.input }));
    const install_app = owner.addInstallFileWithDir(output, .{ .custom = dst_path }, s.output);
    return &install_app.step;
}

/// directory :: String -> BuildFn
/// Build the package in a directory.
///
/// command syntax:
/// <build file>: cd <$project_path>; <builder> [arg]... <$install_prefix> [$cache_dir]
/// <build file>: <builder> [arg]... <$project_path> <$install_prefix> [$cache_dir]
/// e.g.:
/// build.zig: cd $project_path; zig build --prefix $install_prefix --cache-dir $cache_dir
/// Cargo.toml: cargo install --path $project_path --root $install_prefix --target-dir $cache_dir
pub fn directory(cmd: String, owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .directory) return null;

    const dir_path = owner.pathJoin(&.{ opt.sub_path, opt.name }); // relative to current builder
    var dir = owner.build_root.handle.openDir(dir_path, .{}) catch unreachable;
    defer dir.close();

    const colon = mem.indexOfScalar(u8, cmd, ':').?;
    const build_file = cmd[0..colon];
    const build_cmd = cmd[colon + 1 ..];
    dir.access(build_file, .{}) catch return null;

    return build(build_cmd, owner, .{
        .project_path = dir_path,
        .install_prefix = owner.pathJoin(&.{ owner.install_prefix, opt.dst_path orelse opt.sub_path, opt.output_name }),
        .cache_dir = owner.pathJoin(&.{ owner.cache_root.path.?, build_file, opt.sub_path }),
    });
}

/// Build the package in a directory.
/// command syntax:
/// cd <$project_path>; <builder> [arg]... <$install_prefix> [$cache_dir]
/// <builder> [arg]... <$project_path> <$install_prefix> [$cache_dir]
pub fn build(cmd: String, owner: *Build, s: struct {
    project_path: String,
    install_prefix: String,
    cache_dir: String,
}) *Step {
    var it = mem.tokenizeAny(u8, cmd, " \r\n");
    const first_token = it.next().?;
    var builder_name: String = first_token;
    var setcwd: bool = false;

    if (mem.eql(u8, first_token, "cd")) {
        _ = it.next().?;
        builder_name = it.next().?;
        setcwd = true;
    }

    const builder = owner.addSystemCommand(&.{builder_name});
    const dir_path = s.project_path;
    if (setcwd) builder.setCwd(owner.path(dir_path));

    parse: while (it.next()) |token| {
        if (token[0] != '$') {
            builder.addArg(token);
            continue :parse;
        }

        const variable = token[1..];
        inline for (std.meta.fields(@TypeOf(s))) |field| {
            if (mem.eql(u8, variable, field.name)) {
                builder.addArg(@field(s, field.name));
                continue :parse;
            }
        }
        std.log.err("No variable named '{s}' found.", .{token});
        @panic("invalid token");
    }

    builder.setName(owner.fmt("{s} build {s}", .{ builder_name, dir_path }));
    return &builder.step;
}
