const std = @import("std");
const fs = std.fs;
const Build = std.Build;
const Step = Build.Step;
const String = []const u8;

const Options = @import("../build.zig").Options;
const Id = @import("../build.zig").Config.Id;

pub const Lang = enum { c, cc };
pub const Flags = []const String;

/// file :: Lang -> BuildFn
/// Compiles a single source file as an application.
pub fn file(comptime lang: Lang, owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .file) return null;

    const src_ext = "." ++ @tagName(lang);
    if (!std.mem.eql(u8, fs.path.extension(opt.name), src_ext)) return null;

    const exe = owner.addExecutable(.{
        .name = opt.output_name,
        .target = opt.build.target.?,
        .optimize = opt.build.optimize,
    });

    const file_path = owner.pathJoin(&.{ opt.sub_path, opt.name });
    exe.root_module.addCSourceFile(.{
        .file = owner.path(file_path),
        .flags = flags(lang, opt),
    });

    switch (lang) {
        .c => exe.linkLibC(),
        .cc => exe.linkLibCpp(),
    }

    const install_app = owner.addInstallArtifact(exe, .{
        .dest_dir = .{ .override = .{ .custom = opt.dst_path orelse opt.sub_path } },
    });

    return &install_app.step;
}

/// directory :: Lang -> BuildFn
/// Compiles all source code files within a directory into a single application.
pub fn directory(comptime lang: Lang, owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .directory) return null;

    const dir_path = owner.pathJoin(&.{ opt.sub_path, opt.name }); // relative to current builder
    var dir = owner.build_root.handle.openDir(dir_path, .{ .iterate = true }) catch unreachable;
    defer dir.close();
    dir.access("main." ++ @tagName(lang), .{}) catch return null;

    var iter = dir.iterate();
    var file_list: std.ArrayList(String) = .init(owner.allocator);
    defer file_list.deinit();

    const src_ext = "." ++ @tagName(lang);
    while (iter.next() catch unreachable) |entry| {
        if (entry.kind != .file) continue;
        const ext = fs.path.extension(entry.name);
        if (!std.mem.eql(u8, ext, src_ext)) continue;

        file_list.append(owner.pathJoin(&.{ dir_path, entry.name })) catch unreachable;
    }
    if (file_list.items.len == 0) return null;

    const app = owner.addExecutable(.{
        .name = opt.output_name,
        .target = opt.build.target.?,
        .optimize = opt.build.optimize,
    });

    app.root_module.addCSourceFiles(.{
        .files = file_list.items,
        .flags = flags(lang, opt),
    });

    switch (lang) {
        .c => app.linkLibC(),
        .cc => app.linkLibCpp(),
    }

    const install_app = owner.addInstallArtifact(app, .{
        .dest_dir = .{ .override = .{ .custom = opt.dst_path orelse opt.sub_path } },
    });

    return &install_app.step;
}

fn flags(comptime lang: Lang, opt: Options) []const String {
    const id = @field(Id, @tagName(lang));
    const data = opt.build.getData(id);
    if (data == null) return &.{};

    const lang_flags: *const Flags = @alignCast(@ptrCast(data));
    return lang_flags.*;
}
