const std = @import("std");
const fs = std.fs;
const Build = std.Build;
const Step = Build.Step;
const String = []const u8;

const command = @import("command.zig");
const Options = @import("../build.zig").Options;

const src_ext = ".zig";

/// file :: BuildFn
/// Compiles a single source file as an application.
pub fn file(owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .file) return null;
    if (!std.mem.eql(u8, fs.path.extension(opt.name), src_ext)) return null;

    if (std.mem.endsWith(u8, fs.path.stem(opt.name), "_test"))
        return @"test"(owner, opt);

    const file_path = owner.pathJoin(&.{ opt.sub_path, opt.name });
    return buildExe(owner, file_path, opt);
}

/// directory :: BuildFn
/// Compiles all source code files within a directory into a single application.
pub fn directory(owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .directory) return null;

    const dir_path = owner.pathJoin(&.{ opt.sub_path, opt.name }); // relative to current builder
    var dir = owner.build_root.handle.openDir(dir_path, .{}) catch unreachable;
    defer dir.close();

    const build_zig = "build.zig";
    dir.access(build_zig, .{}) catch {
        const main_file = "main.zig";
        dir.access(main_file, .{}) catch return null;

        const file_path = owner.pathJoin(&.{ dir_path, main_file });
        return buildExe(owner, file_path, opt);
    };

    const build_cmd = owner.fmt(
        \\cd $project_path;
        \\zig build
        \\ --prefix $install_prefix
        \\ --cache-dir $cache_dir
        \\ {s}
    ,
        .{if (owner.verbose) "--verbose" else ""},
    );
    return command.build(build_cmd, owner, .{
        .project_path = dir_path,
        .install_prefix = owner.pathJoin(&.{ owner.install_prefix, opt.dst_path orelse opt.sub_path, opt.output_name }),
        .cache_dir = owner.pathJoin(&.{ owner.cache_root.path.?, "zig-build", opt.sub_path }),
    });
}

fn buildExe(owner: *Build, file_path: String, opt: Options) *Step {
    const app = owner.addExecutable(.{
        .name = opt.output_name,
        .root_source_file = owner.path(file_path),
        .target = opt.build.target.?,
        .optimize = opt.build.optimize,
    });

    const install_app = owner.addInstallArtifact(app, .{
        .dest_dir = .{ .override = .{ .custom = opt.dst_path orelse opt.sub_path } },
    });

    return &install_app.step;
}

/// test :: BuildFn
pub fn @"test"(owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .file) return null;
    if (!std.mem.eql(u8, fs.path.extension(opt.name), src_ext)) return null;

    const file_path = owner.pathJoin(&.{ opt.sub_path, opt.name });
    const unit_tests = owner.addTest(.{
        .name = opt.output_name,
        .root_source_file = owner.path(file_path),
        .target = opt.build.target.?,
        .optimize = opt.build.optimize,
    });
    const run_unit_tests = owner.addRunArtifact(unit_tests);
    return &run_unit_tests.step;
}
