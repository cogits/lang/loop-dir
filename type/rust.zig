const std = @import("std");
const fs = std.fs;
const Build = std.Build;
const Step = Build.Step;
const String = []const u8;

const command = @import("command.zig");
const Options = @import("../build.zig").Options;

const src_ext = ".rs";

/// file :: BuildFn
/// Compiles a single source file as an application.
pub fn file(owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .file) return null;
    if (!std.mem.eql(u8, fs.path.extension(opt.name), src_ext)) return null;

    const file_path = owner.pathJoin(&.{ opt.sub_path, opt.name });
    return rustcBuild(owner, file_path, opt);
}

/// directory :: BuildFn
/// Compiles all source code files within a directory into a single application.
pub fn directory(owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .directory) return null;

    const dir_path = owner.pathJoin(&.{ opt.sub_path, opt.name }); // relative to current builder
    var dir = owner.build_root.handle.openDir(dir_path, .{}) catch unreachable;
    defer dir.close();

    const cargo_toml = "Cargo.toml";
    dir.access(cargo_toml, .{}) catch {
        const main_file = "main.rs";
        dir.access(main_file, .{}) catch return null;

        const file_path = owner.pathJoin(&.{ dir_path, main_file });
        return rustcBuild(owner, file_path, opt);
    };

    const build_cmd = owner.fmt(
        \\cargo install
        \\ --path $project_path
        \\ --root $install_prefix
        \\ --target-dir $cache_dir
        \\ {s}
    ,
        .{if (!owner.verbose) "--quiet" else ""},
    );
    return command.build(build_cmd, owner, .{
        .project_path = dir_path,
        .install_prefix = owner.pathJoin(&.{ owner.install_prefix, opt.dst_path orelse opt.sub_path, opt.output_name }),
        .cache_dir = owner.pathJoin(&.{ owner.cache_root.path.?, "cargo", opt.sub_path }),
    });
}

fn rustcBuild(owner: *Build, file_path: String, opt: Options) *Step {
    const opt_level: u8 = switch (opt.build.optimize) {
        .Debug => '0',
        .ReleaseSafe => '2',
        .ReleaseFast => '3',
        .ReleaseSmall => 'z',
    };

    const strip = if (opt.build.strip) |s|
        if (s) "symbols" else "none"
    else switch (opt.build.optimize) {
        .Debug => "none",
        .ReleaseSafe, .ReleaseFast => "debuginfo",
        .ReleaseSmall => "symbols",
    };

    const lto = switch (opt.build.optimize) {
        .Debug => "n",
        .ReleaseSafe => "thin",
        .ReleaseFast, .ReleaseSmall => "y",
    };

    const debug_assertions = switch (opt.build.optimize) {
        .Debug, .ReleaseSafe => "y",
        .ReleaseFast, .ReleaseSmall => "n",
    };

    const target = opt.build.target orelse owner.graph.host;
    const exe_file_ext = target.result.exeFileExt();
    const bin_name = owner.fmt("{s}{s}", .{ opt.output_name, exe_file_ext });

    const build_cmd = owner.fmt(
        \\rustc $input -o $output
        \\ -C opt-level={c}
        \\ -C strip={s}
        \\ -C lto={s}
        \\ -C debug_assertions={s}
    ,
        .{ opt_level, strip, lto, debug_assertions },
    );
    return command.compile(build_cmd, owner, opt.dst_path orelse opt.sub_path, .{
        .input = file_path,
        .output = bin_name,
        .cache_dir = owner.pathJoin(&.{ owner.cache_root.path.?, src_ext, opt.sub_path }),
    });
}
