const std = @import("std");
const fs = std.fs;
const Build = std.Build;
const Step = Build.Step;
const String = []const u8;

const Options = @import("../build.zig").Options;

/// directory :: BuildFn
/// Compiles all source code files within a directory into a single application.
pub fn directory(owner: *Build, opt: Options) ?*Step {
    if (opt.kind != .directory) return null;

    const dir_path = owner.pathJoin(&.{ opt.sub_path, opt.name }); // relative to current builder
    var dir = owner.build_root.handle.openDir(dir_path, .{}) catch unreachable;
    defer dir.close();

    const main_file = "Makefile";
    dir.access(main_file, .{}) catch return null;
    return make(owner, dir_path, opt.dst_path orelse opt.sub_path, opt.output_name);
}

fn make(owner: *Build, src_path: String, dst_path: String, output_name: String) *Step {
    const cmd = owner.addSystemCommand(&.{"make"});
    if (!owner.verbose) cmd.addArg("--quiet");
    cmd.addArgs(&.{ "-C", src_path });
    cmd.setName(owner.fmt("make -C {s}", .{src_path}));
    cmd.setEnvironmentVariable("INSTALL_PREFIX", owner.pathJoin(&.{ owner.install_prefix, dst_path, output_name }));
    return &cmd.step;
}
