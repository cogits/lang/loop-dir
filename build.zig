const std = @import("std");
const fs = std.fs;
const Build = std.Build;
const Entry = fs.Dir.Entry;
const Step = Build.Step;
const String = []const u8;

/// The BuildFn is a callback executed for every file within a directory traversal. Its purpose is
/// to determine whether a build step should be created for the given file. If a build step is not
/// warranted for the current file, the function returns null, and the traversal continues with
/// the next file.
pub const BuildFn = fn (*Build, Options) ?*Step;

pub const Options = struct {
    sub_path: String,
    dst_path: ?String = null,
    name: String,
    kind: fs.File.Kind,
    output_name: String,
    build: Config,
};

pub const Config = struct {
    target: ?Build.ResolvedTarget = null,
    optimize: std.builtin.OptimizeMode = .Debug,
    strip: ?bool = null,
    tls_depth: usize = 0,
    datalist: []const Data = &.{},

    pub const Id = enum(usize) { c, cc, _ };
    pub const Data = struct { Id, *const anyopaque };

    pub fn getData(opt: Config, id: Id) ?*const anyopaque {
        return for (opt.datalist) |data| {
            if (id == data[0]) break data[1];
        } else null;
    }
};

/// Create a build step for a single file or directory, without recursion. This function is designed to
/// handle the compilation of either a single source file or a single directory, as indicated by the
/// last character of the 'src' path. If the 'src' path ends with a '/', it is treated as a directory.
/// Otherwise, it is treated as a file.
pub fn basic(comptime buildFn: BuildFn, owner: *Build, src: String, dst: ?String, config: Config) *Step {
    const name = fs.path.basename(src);

    return buildFn(owner, .{
        .sub_path = fs.path.dirname(src) orelse "",
        .dst_path = if (dst) |d| fs.path.dirname(d) orelse "" else null,
        .name = name,
        .kind = if (src[src.len - 1] == '/') .directory else .file,
        .output_name = if (dst) |d| fs.path.basename(d) else name,
        .build = config,
    }).?;
}

/// Recursively generates build steps for all files and directories within a given path, applying the build
/// function to each entry individually.
pub fn loop(comptime buildFn: BuildFn, owner: *Build, sub_src_path: String, config: Config) ?*Step {
    const sub_path = std.mem.trimRight(u8, sub_src_path, fs.path.sep_str_posix ++ fs.path.sep_str_windows);
    const step_name = name: {
        const str = owner.dupe(sub_path);
        std.mem.replaceScalar(u8, str, fs.path.sep_windows, fs.path.sep_posix);
        break :name str;
    };
    const description = owner.fmt("build {s} directory", .{step_name});

    // nested level
    const s = struct {
        var level: usize = 0;
    };
    s.level += 1;
    defer s.level -= 1;

    const dir_step: *Step = if (config.tls_depth >= s.level)
        owner.step(step_name, description)
    else d: {
        const dummy = owner.allocator.create(struct { step: Step }) catch @panic("OOM!");
        dummy.* = .{
            .step = .init(.{
                .id = .run, // any id would suffice for the dummy step
                .name = description,
                .owner = owner,
            }),
        };
        break :d &dummy.step;
    };

    var dir = owner.build_root.handle.openDir(sub_path, .{ .iterate = true }) catch |err| {
        std.log.err(
            "error occurred when opening directory {s}/{s}",
            .{ owner.build_root.path orelse ".", sub_path },
        );
        @panic(@errorName(err));
    };
    defer dir.close();
    var iter = dir.iterate();

    // iterates over all files in a directory and attempts to apply the buildFn to each one.
    var names: std.StringHashMap(void) = .init(owner.allocator);
    while (iter.next() catch unreachable) |file| {
        if (file.kind != .file and file.kind != .directory) continue;

        const basename = fs.path.stem(file.name);
        const name = if (file.kind == .file and !names.contains(basename) and !exist(dir, basename)) basename else file.name;
        names.put(name, {}) catch unreachable;

        const step = buildFn(owner, .{
            .sub_path = sub_path,
            .name = file.name,
            .kind = file.kind,
            .output_name = owner.dupe(name),
            .build = config,
        }) orelse continue;
        dir_step.dependOn(step);
    }

    return if (dir_step.dependencies.items.len == 0) null else dir_step;
}

fn exist(dir: fs.Dir, sub_path: []const u8) bool {
    dir.access(sub_path, .{}) catch return false;
    return true;
}

/// curry :: f -> a -> BuildFn
/// takes the first argument and returns BuildFn which accepts further arguments.
pub fn curry(func: anytype, first_arg: anytype) BuildFn {
    return struct {
        pub fn buildFn(owner: *Build, opt: Options) ?*Step {
            return func(first_arg, owner, opt);
        }
    }.buildFn;
}

/// mconcat :: Monoid BuildFn => [BuildFn] -> BuildFn
/// combines multiple BuildFn functions into a single BuildFn function.
pub fn mconcat(funcs: anytype) BuildFn {
    return struct {
        /// sequentially runs each BuildFn and returns the first non-null step.
        pub fn buildFn(owner: *Build, opt: Options) ?*Step {
            var step: ?*Step = undefined;
            inline for (funcs) |func| {
                step = func(owner, opt);
                if (step != null) return step;
            }
            return null;
        }
    }.buildFn;
}

/// mempty :: BuildFn
pub fn mempty(_: *Build, _: Options) ?*Step {
    return null;
}

/// Recursive :: BuildFn -> BuildFn -> BuildFn
/// Takes two distinct BuildFns, one for processing files and another for processing directories,
/// and returns a recursive BuildFn that compiles all source files and directories recursively.
pub fn recursive(comptime file: BuildFn, comptime directory: BuildFn) BuildFn {
    return struct {
        pub fn rec(owner: *Build, opt: Options) ?*Step {
            if (opt.kind == .file) return file(owner, opt);
            return directory(owner, opt) orelse
                loop(rec, owner, owner.pathJoin(&.{ opt.sub_path, opt.name }), opt.build);
        }
    }.rec;
}

pub const make = @import("type/make.zig");
pub const cc = @import("type/cc.zig");
pub const zig = @import("type/zig.zig");
pub const rust = @import("type/rust.zig");
pub const command = @import("type/command.zig");

pub const buildFileC: BuildFn = curry(cc.file, .c);
pub const buildFileCPP: BuildFn = curry(cc.file, .cc);
pub const buildFileC_CPP: BuildFn = mconcat(.{ buildFileC, buildFileCPP });
pub const buildFileZig: BuildFn = zig.file;
pub const buildFileRust: BuildFn = rust.file;
pub const buildFileAll: BuildFn = mconcat(.{
    buildFileC_CPP,
    buildFileZig,
    buildFileRust,
});

pub const buildDirMake: BuildFn = make.directory;
pub const buildDirC: BuildFn = curry(cc.directory, .c);
pub const buildDirCPP: BuildFn = curry(cc.directory, .cc);
pub const buildDirC_CPP: BuildFn = mconcat(.{ buildDirC, buildDirCPP });
pub const buildDirZig: BuildFn = zig.directory;
pub const buildDirRust: BuildFn = rust.directory;
pub const buildDirAll: BuildFn = mconcat(.{
    buildDirMake,
    buildDirC_CPP,
    buildDirZig,
    buildDirRust,
});

pub const buildMake: BuildFn = recursive(mempty, buildDirMake);
pub const buildC: BuildFn = recursive(buildFileC, buildDirC);
pub const buildCPP: BuildFn = recursive(buildFileCPP, buildDirCPP);
pub const buildC_CPP: BuildFn = recursive(buildFileC_CPP, buildDirC_CPP);
pub const buildZig: BuildFn = recursive(buildFileZig, buildDirZig);
pub const buildRust: BuildFn = recursive(buildFileRust, buildDirRust);
pub const buildAll: BuildFn = recursive(buildFileAll, buildDirAll);

pub const testZig: BuildFn = zig.@"test";

pub fn build(_: *Build) void {}
