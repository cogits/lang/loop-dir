# loop-dir

This build system module is designed to recursively traverse directories and apply a BuildFn function to compile each file individually.

It streamlines the compilation process for small to medium-sized projects in languages like C, C++, Zig, Rust, and Make. With built-in support for common project types and the flexibility to add custom commands, it simplifies the build process for developers.

To see how it works, check out the example directory for guidance on integrating it into your projects.
